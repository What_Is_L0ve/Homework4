﻿namespace Common.Interfaces;

public interface ICustomSerializerService
{
    public string Serialize<T>(T value);

    T? Deserialize<T>(string value) where T : new();
}