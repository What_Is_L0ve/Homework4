﻿using Common.Models;

namespace Common.Interfaces;

public interface IDataContainerFactory
{
    DataContainer Create();
}