﻿namespace Common.Interfaces;

public interface ISerializerTestService
{
    void Test(string testName, bool writeProcess, int iterationCount = 1);
}