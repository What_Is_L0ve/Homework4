﻿using Common.Implementations;
using Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Common;

public static class DependencyInjection
{
    public static IServiceProvider ConfigureServices()
    {
        var services = new ServiceCollection();
        services.AddSingleton<IDataContainerFactory, DataContainerFactory>();
        services.AddSingleton<ICustomSerializerService, CustomSerializerService>();
        services.AddSingleton<ISerializerTestService, CustomSerializerTestService>();
        services.AddSingleton<SystemTextJsonSerializerTestService>();

        return services.BuildServiceProvider();
    }
}