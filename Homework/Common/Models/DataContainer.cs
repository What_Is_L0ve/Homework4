﻿namespace Common.Models;

public class DataContainer
{
    public int I5;

    public DataContainer(int i1, int i2, int i3, int i4, int i5)
    {
        I1 = i1;
        I2 = i2;
        I3 = i3;
        I4 = i4;
        I5 = i5;
    }

    public DataContainer()
    {
    }

    public int I1 { get; set; }

    public int I2 { get; set; }

    public int I3 { get; set; }

    public int I4 { get; set; }
}