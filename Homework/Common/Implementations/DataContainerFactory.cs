﻿using Common.Interfaces;
using Common.Models;

namespace Common.Implementations;

public class DataContainerFactory : IDataContainerFactory
{
    public DataContainer Create()
    {
        return new(1, 2, 3, 4, 5);
    }
}