﻿using System.Diagnostics;
using Common.Interfaces;
using Common.Models;

namespace Common.Implementations;

public class CustomSerializerTestService : ISerializerTestService
{
    private readonly ICustomSerializerService _customSerializerService;

    private readonly DataContainer _dataContainer;

    public CustomSerializerTestService(IDataContainerFactory containerFactory,
        ICustomSerializerService customSerializerService)
    {
        _customSerializerService = customSerializerService;
        _dataContainer = containerFactory.Create();
    }

    public void Test(string testName, bool writeProcess = false, int iterationCount = 1)
    {
        Console.WriteLine(testName);
        try
        {
            var serializeString = TestOperation("Сериализация", SerializeTest, writeProcess, iterationCount);
            var deserializeString = TestOperation("Десериализация", DeserializeTest, writeProcess, iterationCount);
            Console.WriteLine();
            Console.WriteLine($"{testName}, результаты теста: ");
            Console.WriteLine($"Количество итераций: {iterationCount}");
            Console.WriteLine($"Выводить данные на консоль: {writeProcess}");
            Console.WriteLine(serializeString);
            Console.WriteLine(deserializeString);
        }
        catch (Exception exception)
        {
            Console.WriteLine($"Произошла ошибка при выполнении теста {testName} : {exception.Message}");
        }

        Console.WriteLine();
    }

    private string TestOperation(string testName, Action<bool, int> action, bool writeProcess = true,
        int iterationCount = 1)
    {
        Console.WriteLine(testName);
        var timer = new Stopwatch();
        timer.Start();
        action.Invoke(writeProcess, iterationCount);
        timer.Stop();
        var timeString = $"{testName} время: {timer.Elapsed:m\\:ss\\.fff}";
        Console.WriteLine($"{testName} завершена");

        return timeString;
    }

    private void SerializeTest(bool writeProcess = true, int iterationCount = 1)
    {
        for (var i = 0; i < iterationCount; i++)
        {
            var resultString = _customSerializerService.Serialize(_dataContainer);
            if (writeProcess)
            {
                Console.WriteLine(resultString);
            }
        }
    }

    private void DeserializeTest(bool writeProcess = true, int iterationCount = 1)
    {
        var str = _customSerializerService.Serialize(_dataContainer);
        for (var i = 0; i < iterationCount; i++)
        {
            var value = _customSerializerService.Deserialize<DataContainer>(str);
            if (writeProcess)
            {
                Console.WriteLine($"I1:{value?.I1}, I2:{value?.I2}, I3:{value?.I3}, I4:{value?.I4}, I5:{value?.I5}");
            }
        }
    }
}