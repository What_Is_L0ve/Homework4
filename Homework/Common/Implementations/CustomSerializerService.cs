﻿using Common.Interfaces;

namespace Common.Implementations;

public class CustomSerializerService : ICustomSerializerService
{
    private const string JoinSeparator = ";";
    private const string ValueSeparator = ":";

    public string Serialize<T>(T value)
    {
        var type = typeof(T);
        var fields = type.GetFields();
        var list = fields.Select(field => $"{field.Name}{ValueSeparator}{field.GetValue(value)}").ToList();
        var properties = type.GetProperties();
        list.AddRange(properties.Select(property => $"{property.Name}{ValueSeparator}{property.GetValue(value)}"));

        return string.Join(JoinSeparator, list.ToArray());
    }

    public T? Deserialize<T>(string value) where T : new()
    {
        if (string.IsNullOrEmpty(value))
        {
            return default;
        }

        var parts = value.Split(JoinSeparator);
        var dictionary = parts.Select(part => part.Split(ValueSeparator))
            .ToDictionary(keyValue => keyValue[0], keyValue => int.Parse(keyValue[1]));

        var result = new T();
        var type = typeof(T);
        var fields = type.GetFields();
        foreach (var field in fields)
        {
            field.SetValue(result, dictionary[field.Name]);
        }

        var properties = type.GetProperties();
        foreach (var field in properties)
        {
            field.SetValue(result, dictionary[field.Name]);
        }

        return result;
    }
}