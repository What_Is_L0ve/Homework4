﻿using System.Diagnostics;
using System.Text.Json;
using Common.Interfaces;
using Common.Models;

namespace Common.Implementations;

public class SystemTextJsonSerializerTestService : ISerializerTestService
{
    private readonly DataContainer _dataContainer;

    public SystemTextJsonSerializerTestService(IDataContainerFactory containerFactory)
    {
        _dataContainer = containerFactory.Create();
    }

    public void Test(string testName, bool writeProcess = false, int iterationCount = 1)
    {
        Console.WriteLine(testName);
        try
        {
            var serializeString = TestOperation("Сериализация", SerializeTest, writeProcess, iterationCount);
            var deserializeString = TestOperation("Десериализация", DeserializeTest, writeProcess, iterationCount);
            Console.WriteLine();
            Console.WriteLine($"{testName}, результаты теста: ");
            Console.WriteLine($"Количество итераций: {iterationCount}");
            Console.WriteLine($"Выводить данные на консоль: {writeProcess}");
            Console.WriteLine(serializeString);
            Console.WriteLine(deserializeString);
        }
        catch (Exception exception)
        {
            Console.WriteLine($"Произошла ошибка при выполнении теста {testName} : {exception.Message}");
        }

        Console.WriteLine();
        Console.ReadKey();
    }

    private string TestOperation(string testName, Action<bool, int> action, bool writeProcess = true,
        int iterationCount = 1)
    {
        Console.WriteLine(testName);
        var timer = new Stopwatch();
        timer.Start();
        action.Invoke(writeProcess, iterationCount);
        timer.Stop();
        var timeString = $"{testName} время: {timer.Elapsed:m\\:ss\\.fff}";
        Console.WriteLine($"{testName} завершена");

        return timeString;
    }

    private void SerializeTest(bool writeProcess = true, int iterationCount = 1)
    {
        for (var i = 0; i < iterationCount; i++)
        {
            var resultString = JsonSerializer.Serialize(_dataContainer);
            if (writeProcess)
            {
                Console.WriteLine(resultString);
            }
        }
    }

    private void DeserializeTest(bool writeProcess = true, int iterationCount = 1)
    {
        var json = JsonSerializer.Serialize(_dataContainer);
        for (var i = 0; i < iterationCount; i++)
        {
            var value = JsonSerializer.Deserialize<DataContainer>(json);
            if (writeProcess)
            {
                Console.WriteLine($"I1:{value?.I1}, I2:{value?.I2}, I3:{value?.I3}, I4:{value?.I4}, I5:{value?.I5}");
            }
        }
    }
}