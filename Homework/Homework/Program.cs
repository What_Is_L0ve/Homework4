﻿using Common;
using Common.Implementations;
using Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Homework;

internal static class Program
{
    private const int IterationCount = 100_000;
    private const bool WriteProcess = false;

    private static void Main()
    {
        Console.Clear();
        var provider = DependencyInjection.ConfigureServices();
        var customSerializerTestService = provider.GetRequiredService<ISerializerTestService>();
        var systemTextJsonSerializerTestService = provider.GetRequiredService<SystemTextJsonSerializerTestService>();
        customSerializerTestService.Test("Кастомный сериализатор", WriteProcess, IterationCount);
        systemTextJsonSerializerTestService.Test("System.Text.Json", WriteProcess, IterationCount);
    }
}